<?php

$myStr=addslashes ('Hello \37\ Hello \'37\' Hello "37" Hello "37"');
echo $myStr."<br>";


$myStr = "Hello World! How is life?";

$myArr = explode(" ",$myStr);
print_r($myArr);
echo "<br>";


$myStr = implode("*",$myArr);
echo $myStr."<br>";

$myStr = "<br> means line break";
$myStr = htmlentities($myStr);
echo $myStr;


$myStr= "  Hello World  ";
echo trim($myStr);
echo "<br>";

$myStr= "  Hello World  ";
echo ltrim($myStr);
echo "<br>";

$myStr= "  Hello World  ";
echo rtrim($myStr);
echo "<br>";



echo "\n\n\n\n\nHi There!";
$myStr = nl2br($myStr);
echo $myStr;

echo "<br> <br>";

$mainStr = "Hello World";

$padStr = str_pad($mainStr,50,"mainStr");
echo $padStr;
echo "<br> <br>";

$mainStr = "Hello World";

$repeatedStr = str_repeat($mainStr,5);
echo $repeatedStr;



echo "<br> <br>";
$mainStr = "Hello World";
$replacedStr = str_replace("o","O",$mainStr);
echo $replacedStr;


echo "<br> <br>";
$mainStr = "Hello World";

$myArr = str_split($mainStr);
print_r($myArr);

echo "<br> <br>";
$mainStr = "Hello World";
$subStr = "Hello";
echo substr_compare($mainStr,$subStr,0);




echo "<br> <br>";
$mainStr = "Hello World Hello World Hello World Hello World";
$subStr = "Hello";
echo substr_count($mainStr,$subStr);


echo "<br> <br>";
$mainStr = "Hello World Hello World Hello World Hello World";
$subStr = "Hi";
echo substr_replace($mainStr,$subStr,10);


echo "<br> <br>";
$mainStr = "Hello World";
echo ucfirst($mainStr);


echo "<br> <br>";
$mainStr = "Hello World";
echo ucwords($mainStr);