<?php

// Boolean example is started from here
 $decision =true;
 if($decision) echo "the decision is true";

 $decision = false;
 if($decision) echo "the decision is false";
 echo "<br>";
// Boolean example is ended here


// Integer example is started here

$value = 100;
echo $value;
echo "<br>";

// Integer example is ended here


// Float/Double example is started from here

$value = 38.56;
echo $value;
echo "<br>";

// Float/Double example is ended here


// string example start here

  $var = 100;

  $string1 = 'This is a single quoted string $var <br>';
  $string2 = "This is a double quoted string $var <br>";
 echo $string1.$string2;

$heredocString =<<<BITM
    this is a heredoc example lin1 $var <br>
    this is a heredoc example lin2 $var <br>
    this is a heredoc example lin3 $var <br>
    this is a heredoc example lin4 $var <br>
    this is a heredoc example lin5 $var <br>
BITM;


$nowdocString =<<<'BITM'
    this is a nowdoc example lin1 $var <br>
    this is a nowdoc example lin2 $var <br>
    this is a nowdoc example lin3 $var <br>
    this is a nowdoc example lin4 $var <br>
    this is a nowdoc example lin5 $var <br>
BITM;

echo $heredocString . "<br> <br>" . $nowdocString;


// string example ended here




$arr = array(1,2,3,4,5,6,7,8,9,10);
print_r($arr);
echo "<br>";
$arr= array("BMW","FERRARI","TOYOTA","NISSAN");
print_r($arr);
echo "<br>";


$ageArray = array("Arif"=>30,"Moynar Ma=>45","Rahim"=>25);
print_r($ageArray);

echo "<br>";

echo "the age of Moynar Ma" . $ageArray["Moynar Ma"];

echo "<br>";
